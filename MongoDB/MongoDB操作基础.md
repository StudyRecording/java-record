>创建时间: 2022-03-16 16:47
>修改时间: <%+ tp.file.last_modified_date() %>

#MongoDB #基础

# MongoDB操作基础
## MongoDB与数据库概念对比
| MongoDB           | MySQL数据库 |
| ----------------- | ----------- |
| 数据库(database)  | 数据库      |
| 集合(collection)  | 表          |
| 文档(document)    | 行          |
| 字段(field)       | 列          |
| 索引(index)       | 索引        |
| \_id              | 主键        |
| 视图(view)        | 视图        |
| 聚合操作($lookup) | 表连接      |

## 常用命令
- 显示数据库列表: `show dbs`
- 切换数据库，如果没有则新建：`use {数据库名}`
- 删除当前数据库：`db.dropDatabase()`
- 显示当前数据库的集合列表：`show collections`
- 查看集合详情：`db.{集合名}.stats()`
- 删除集合：`db.{集合名}.drop()`
- 显示当前数据库的用户列表：`show users`
- 显示当前数据库的绝设列表：`show roles`
- 显示最近发生的操作：`show profile`
- 执行一个JavaScript脚本：`load("{脚本路径/脚本名}")`

## 常用业务命令
1. 创建集合
	`db.createCollection({集合名}, {option})`
	**option:**
	```json
	{
	  capped: false, # 布尔类型，true-固定集合；false-一般集合
	  size: 1024, # 为固定集合指定一个最大值（以字节计）， 如果 capped 为 true，也需要指定该字段。
	  max: 1000 # 指定集合中文档的最大数量
	}
	```



