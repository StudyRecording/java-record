# Docker部署RabbitMQ镜像集群
```shell
#拉取镜像，指定带有“mangement”的版本（包含web管理页面），该版本包含了web控制页面
docker pull rabbitmq:management


docker run -d --hostname myRabbit1 --name rabbit1 -p 15672:15672 -p 4369:4369 -p 5672:5672 -e RABBITMQ_ERLANG_COOKIE='rabbitcookie' rabbitmq:management

docker run -d --hostname myRabbit2 --name rabbit2 -p 5673:5672 -p 4370:4369 --link rabbit1:myRabbit1 -e RABBITMQ_ERLANG_COOKIE='rabbitcookie' rabbitmq:management

docker run -d --hostname myRabbit3 --name rabbit3 -p 5674:5672 -p 4371:4369 --link rabbit1:myRabbit1 --link rabbit2:myRabbit2 -e RABBITMQ_ERLANG_COOKIE='rabbitcookie' rabbitmq:management

```

```shell
#进入rabbitmq01容器，重新初始化一下，如果是新安装则reset可以忽略重置。
docker exec -it rabbit1 bash
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl start_app
exit

#进入rabbitmq02容器，重新初始化一下，将02节点加入到集群中
docker exec -it rabbit2 bash
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl join_cluster --ram rabbit@myRabbit1 #参数“--ram”表示设置为内存节点，忽略该参数默认为磁盘节点。
rabbitmqctl start_app
exit

#进入rabbitmq03容器，重新初始化一下，将03节点加入到集群中
docker exec -it rabbit3 bash
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl join_cluster --ram rabbit@myRabbit1
rabbitmqctl start_app
exit

```

```shell
#任意节点执行，设置镜像集群模式
rabbitmqctl set_policy -p / ha "^" '{"ha-mode":"all","ha-sync-mode":"automatic"}'

#查询策略 
#查看vhost下的所有的策略（policies ）
rabbitmqctl list_policies -p / 

#查看集群的状态:
rabbitmqctl cluster_status

#查看镜像队列  
`rabbitmqctl list_policies`  
#删除镜像队列  
`rabbitmqctl clear_policy`
```