```shell
docker run --name zookeeper --restart always -d -v $(pwd)/zoo.cfg:/conf/zoo.cfg -v $(pwd)/data:/opt/zookeeper/data -p 2181:2181 zookeeper
```