```shell
# 垃取镜像
docker pull mongo

# 创建镜像
docker run --name mongo-server -v /Users/hpc/DockerFileSystem/MongoDB/db:/data/db -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=root -d mongo
```

进入mongo-server执行命令
![[Pasted image 20220313181127.png]]