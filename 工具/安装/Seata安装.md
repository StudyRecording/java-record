#Tool #Seata #安装
# Seata安装以及Spring boot集成

Seata使用docker安装的方式
**官方文档**： [Seata部署指南](http://seata.io/zh-cn/docs/ops/deploy-guide-beginner.html)

**前提：** Nacos安装成功，具有docker环境

## 安装的三个角色(GitHub中的三个目录)

**Github地址: ** [seata/script at 1.4.0 · seata/seata (github.com)](https://github.com/seata/seata/tree/1.4.0/script)

- client: 可以理解为集成Seata的Spring boot程序。在github中该目录中主要存放`undo_log.sql`文件和项目的配置文件，其中`undo_log.sql`文件主要在业务数据库中执行存放。
- config-serverd: 这里可以理解为配置中心，也就时Nacos。在github中该目录主要存放各种配置中心需要导入的配置，这里以Nacos为例子：`config.txt`时需要导入的配置，`nacos-config.sh`或`nacos-config.py`是可以将`config.txt`导入Nacos的脚本。
- server： 这里理解为Seata Server 部署的环境。在github中该目录中保存的是需要执行的数据库文件。（注意，这些脚本与`undo_log.sql`不同，它们不放在业务数据库中）

## Seata Server 部署

### 建立MySQL环境

在数据库中新建seata的数据库，并执行一下的SQL文件:

[seata/mysql.sql at 1.4.0 · seata/seata (github.com)](https://github.com/seata/seata/blob/1.4.0/script/server/db/mysql.sql)

### 编写配置文件

- 获取配置文件

  在GitHub中下载Seat Server Release文件(这里下载仅仅是为了获取配置文件，关于部署将使用docker方式)，然后解压文件：

  ```shell
  # 下载文件
  wget -c https://github.com/seata/seata/releases/download/v1.4.2/seata-server-1.4.2.zip
  # 解压文件
  unzipseata-server-1.4.2.zip
  ```

- 将配置文件报错到自己定义的目录中

  ```shell
  # 新建目录
  mkdir /home/{user}/seata/config
  # 将配置文件放入到自定义目录中
  cp /home/{user}/seata/seata/seata-server-1.4.2/config/file.conf /home/{user}/seata/config
  cp /home/{user}/seata/seata/seata-server-1.4.2/config/registry.conf /home/{user}/seata/config
  ```

- 修改配置文件file.conf

  ```
  # transaction log store, only used in seata-server
  store {
    ## store mode: file、db、redis, 
    mode = "db" # 这里修改为db
    ## rsa decryption public key
    publicKey = ""
    ......
  ## database store property 在这里完善Seata将使用的数据库相关属性
    db {
      ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp)/HikariDataSource(hikari) etc.
      datasource = "druid"
      ## mysql/oracle/postgresql/h2/oceanbase etc.
      dbType = "mysql" # 表示使用mysql数据库
      driverClassName = "com.mysql.cj.jdbc.Driver" 
      ## if using mysql to store the data, recommend add rewriteBatchedStatements=true in jdbc connection param
      url = "jdbc:mysql://192.168.1.1:3306/seata?rewriteBatchedStatements=true&serverTimezone=Asia/Shanghai&characterEncoding=utf-8&useSSL=false"
      user = "root"
      password = "root"
      minConn = 5
      maxConn = 100
      globalTable = "global_table"
      branchTable = "branch_table"
      lockTable = "lock_table"
      queryLimit = 100
      maxWait = 5000
    }
    ......
  }
  ```

- 修改配置的registry.conf文件

  ```
  registry {
    # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
    type = "nacos" # 因为使用的配置中心是Nacos，所以这里选择nacos
  
    # 这里填写nacos的相关属性
    nacos {
      application = "seata-server"
      serverAddr = "192.168.1.1:8848"
      group = "SEATA_GROUP"
      namespace = ""
      cluster = "default"
      username = "nacos"
      password = "nacos"
    }
    ......
  }
  config {
    # file、nacos 、apollo、zk、consul、etcd3
    type = "file"
    ......
    file {
    	# 这里说名一下，因为使用docker部署的话没有命令直接配置file.conf
    	# 文件，因此直接在registry.conf里生命file.conf文件的位置
    	#（这里file.conf的位置是在docker容器内的位置，
    	# 在执行docker名令时会将实际的自定义配置文件目录与docker环境中的目录进行映射）
      name = "file:/root/seata-config/file.conf"
    }
  }
  ```

### 在Nacos中添加Seata相关配置

- 修改config.txt文件

  ```tex
  transport.type=TCP
  transport.server=NIO
  transport.heartbeat=true
  transport.enableClientBatchSendRequest=false
  transport.threadFactory.bossThreadPrefix=NettyBoss
  transport.threadFactory.workerThreadPrefix=NettyServerNIOWorker
  transport.threadFactory.serverExecutorThreadPrefix=NettyServerBizHandler
  transport.threadFactory.shareBossWorker=false
  transport.threadFactory.clientSelectorThreadPrefix=NettyClientSelector
  transport.threadFactory.clientSelectorThreadSize=1
  transport.threadFactory.clientWorkerThreadPrefix=NettyClientWorkerThread
  transport.threadFactory.bossThreadSize=1
  transport.threadFactory.workerThreadSize=default
  transport.shutdown.wait=3
  # 修改下一行，记住修改的东东，后面要用
  # service.vgroupMapping.my_test_tx_group=default
  service.vgroupMapping.default_tx_group=default
  # 这里需要修改为 Seata的网络访问地址
  # service.default.grouplist=127.0.0.1:8091
  service.default.grouplist=192.168.1.1:8091
  service.enableDegrade=false
  service.disableGlobalTransaction=false
  client.rm.asyncCommitBufferLimit=10000
  client.rm.lock.retryInterval=10
  client.rm.lock.retryTimes=30
  client.rm.lock.retryPolicyBranchRollbackOnConflict=true
  client.rm.reportRetryCount=5
  client.rm.tableMetaCheckEnable=false
  client.rm.sqlParserType=druid
  client.rm.reportSuccessEnable=false
  client.rm.sagaBranchRegisterEnable=false
  client.tm.commitRetryCount=5
  client.tm.rollbackRetryCount=5
  client.tm.defaultGlobalTransactionTimeout=60000
  client.tm.degradeCheck=false
  client.tm.degradeCheckAllowTimes=10
  client.tm.degradeCheckPeriod=2000
  # 这里需要修改成db
  # store.mode=file
  store.mode=db
  store.file.dir=file_store/data
  store.file.maxBranchSessionSize=16384
  store.file.maxGlobalSessionSize=512
  store.file.fileWriteBufferCacheSize=16384
  store.file.flushDiskMode=async
  store.file.sessionReloadReadSize=100
  # 接下来修改为自己数据库的信息
  store.db.datasource=druid
  store.db.dbType=mysql
  store.db.driverClassName=com.mysql.cj.jdbc.Driver
  store.db.url=jdbc:mysql://192.168.1.1:3306/seata?useUnicode=true&rewriteBatchedStatements=true&serverTimezone=Asia/Shanghai&characterEncoding=utf-8&useSSL=false
  store.db.user=root
  store.db.password=root
  store.db.minConn=5
  store.db.maxConn=30
  store.db.globalTable=global_table
  store.db.branchTable=branch_table
  store.db.queryLimit=100
  store.db.lockTable=lock_table
  store.db.maxWait=5000
  store.redis.host=127.0.0.1
  store.redis.port=6379
  store.redis.maxConn=10
  store.redis.minConn=1
  store.redis.database=0
  store.redis.password=null
  store.redis.queryLimit=100
  server.recovery.committingRetryPeriod=1000
  server.recovery.asynCommittingRetryPeriod=1000
  server.recovery.rollbackingRetryPeriod=1000
  server.recovery.timeoutRetryPeriod=1000
  server.maxCommitRetryTimeout=-1
  server.maxRollbackRetryTimeout=-1
  server.rollbackRetryTimeoutUnlockEnable=false
  client.undo.dataValidation=true
  client.undo.logSerialization=jackson
  client.undo.onlyCareUpdateColumns=true
  server.undo.logSaveDays=7
  server.undo.logDeletePeriod=86400000
  client.undo.logTable=undo_log
  client.log.exceptionRate=100
  transport.serialization=seata
  transport.compressor=none
  metrics.enabled=false
  metrics.registryType=compact
  metrics.exporterList=prometheus
  metrics.exporterPrometheusPort=9898
  ```

- 将配置文件上传到Nacos配置中心中

  ```shell
  # 获取nacos-config.sh文件: https://github.com/seata/seata/blob/1.4.0/script/config-center/nacos/nacos-config.sh
  # 为文件授予执行权限
  sudo chmod +x nacos-config.sh
  # 执行下述命令, 其中-u后是nacos的用户名, -w是nacos的密码
  ./nacos-config.sh -h 192.168.1.1 -p 8848 -g SEATA_GROUP -u nacos -w nacos
  ```

### docker启动Seata Server环境

```shell
docker run --name seata-server -p 8091:8091 -e SEATA_IP=192.168.1.1 -e STORE_MODE=db -e SEATA_CONFIG_NAME=file:/root/seata-config/registry -v /home/{user}/seate/config:/root/seata-config -d seataio/seata-server:1.4.2
```

其中 `SEATA_CONFIG_NAME`获取的是docker容器中registry.conf配置文件的位置

`-v /home/{user}/seate/config:/root/seata-config`是将本地的`/home/{user}/seate/config`的目录挂载到`/root/seata-config`目录中。

## Spring Boot集成Seata环境

**前提：** 数据库和Mybatis plus已经集成上了，项目单独能够正常的运行。

### 业务数据库建表

```sql
-- for AT mode you must to init this sql for you business database. the seata server not need it.
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT(20)   NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(100) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8 COMMENT ='AT transaction mode undo table';
```


### 引用pom依赖

```xml
<dependency>
    <groupId>io.seata</groupId>
    <artifactId>seata-spring-boot-starter</artifactId>
    <version>${seata-boot.version}</version>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
    <version>${seata-cloud.version}</version>
    <exclusions>
        <exclusion>
            <groupId>io.seata</groupId>
            <artifactId>seata-spring-boot-starter</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

### yml配置文件

```yaml

## seata配置
seata:
  enabled: true
  application-id: ${spring.application.name}
  # 下面这个需要跟刚刚config.txt上传到nacos配置中心的相同
  # config.txt中：service.vgroupMapping.default_tx_group=default（自己看，自己理解）
  tx-service-group: default_tx_group 
  # 这里开启自动的数据源配置，否则后秒要自己手动配置
  enable-auto-data-source-proxy: true
  data-source-proxy-mode: AT
  use-jdk-proxy: false
  registry:
    type: nacos
    nacos:
      application: seata-server
      server-addr: 192.168.1.1:8848
      group : "SEATA_GROUP"
      namespace: ""
      username: "nacos"
      password: "nacos"
  config:
    type: nacos
    nacos:
      server-addr: 192.168.1.1:8848
      namespace: ""
      group: "SEATA_GROUP"
      username: "nacos"
      password: "nacos"
  log:
    exception-rate: 100
  tcc-fence:
    enable: true
    config:
      log-table-name: tcc_fence_log
      clean-mode: hour
      clean-period: 1
```

