#Tool #frp #内网穿透 #安装
# frp内网穿透

## 官方文档地址

[文档 | frp (gofrp.org)](https://gofrp.org/docs/)

## 快速使用

frp的使用主要分为两个端，分别时服务器端和客户端，其中客户端也就是被内网穿透的电脑或服务器，而服务器端则是具有公网ip的可以进行内网穿透的电脑或服务器。

在这里仅记录简单快速的使用方式

- 服务器端

  首先需要下载frp的Release的包，然后解压并进入到解压目录，然后会发现有文件：`frps frps.ini`

  这两个文件便是内网穿透服务端的主要工具。然后编辑`frps.ini`文件，如下：

  ```ini
  [common]
  bind_port = 3000       # 这个端口是服务器监听被内网穿透客户端的
  vhost_http_port = 1111 # 这个是在http上需要用到，但我没有域名，便没有用，理论上如果想要访问网址的化也可以使用tcp的映射，通过ip和端口进行访问
  dashboard_port = 7234  # 服务的管理界面
  dashboard_user = username #服务端管理界面登录用户名
  dashboard_pwd = password  # 服务端管理界面登录密码
  ```

  编辑完成上述文件后，通过`./frps -c ./frps.ini`执行可以启动内网穿透服务器。当然此时一旦退出终端，内网穿透便会停止，一次最好启动命令时，程序的运行时后台运行不依赖终端的，命令如下:

  ```shell
  mkdir log.log # 新建日志文件
  nohup ./frps -c ./frps.ini > log.log 2>&1 & # 此命令执行后，内网穿透后台运行
  ```

  启动成功后便可以访问服务端的管理界面，如下图：

  **访问地址**： 公网ip:dashboar_port

![[frps.png]]

- 被内网穿透的客户端

  同样首先下载frp包，然后解压，能在里面看到两个文件:`frpc  frpc.ini`

  然后编辑`frpc.ini`的内容，如下：

  ```ini
  [common]
  server_addr = x.x.x.x      # 代理服务器的ip
  server_port = 3000         # 代理服务器接受内网信息的接口
  admin_addr = 127.0.0.1     # 被代理客户端的管理界面地址
  admin_port = 4400          # 被代理客户端界面的port
  admin_user = username     # 被代理客户端界面的用户名
  admin_pwd = password      # 被代理客户端界面的用户密码
  
  [ssh]                           # 自定义一个内网穿透名称
  type = tcp                      # 内网穿透方式
  local_ip = 127.0.0.1            # 内网应用程序的sport
  local_port = 22                # 内网客户端上应用程序的核好
  remote_port = 22222            # 内网穿透服务器需要开放的端口，用户可以经过这个端口进行请求，然后将相关信息再发送到用户手中
  
  [admin_ui]                   # 客户端的管理页面
  type = tcp
  local_port = 1234
  remote_port = 1234
  
  [nacos]
  type = tcp
  local_port = 1234
  remote_port = 1234
  ```

  如上述文件，主要采用的都是port进行连接

​       然后通过命令

```shell
mkdir log.log
nohup ./frpc -c ./frpc.ini > log.log 2>&1 &
```

经过这段命令的执行，内网穿透就大体已经实现了，客户端管理界面如下：

![[frpc.png]]

**访问地址**: 公网IP:admin_port

这个页面主要是看有那些穿透程序和端口的，在`Configur`菜单页面中，是可以直接配置frpc.ini文件，从而实现线上添加或删除端口的内网穿透实例。