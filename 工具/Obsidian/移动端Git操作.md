#Tool #Obsidian #Android #使用 
1. 进入Termux应用
2. 输入命令`cd /sdcard`
3. 如果存在**java-record**目录，则进入该文件夹；如果不存在，可使用git克隆该库，然后进入**java-record**目录
	```ad-quote
	git工具下载命令：pkg install git
	```
4. 如果第一次克隆该仓库，则使用移动端的Obsidian打开**java-record**目录便可; 如果原本便存在该目录，则进入该目录后执行命令`git pull`，然后使用过Obsidian打开该目录
