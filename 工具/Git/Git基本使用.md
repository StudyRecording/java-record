#Tool #Git #使用 
## 撤销
1. 撤销未add的文件
	```shell
	git reset --hard HEAD
	```
1. 查看所有分支`git branch -a`
2. 配置git仓库本地用户名
	```shell
	git config --local user.name "pchu01"
	git config --local user.email "pchu01@mail.yst.com.cn"
	```