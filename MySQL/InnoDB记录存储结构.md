#MySQL #InnoDB #存储结构
# InnoDB页

InnoDB会将数据划分为若干个页，以页作为磁盘和内存之间交互的基本单位。InnoDB中页的大小一般为16KB。（系统变量`innodb_page_size`表明了InnoDB存储引擎中的页的大小）

# InnoDB行格式

平时以记录为单位向表中插入数据，这些记录在磁盘上的存放形式被称为行格式或者记录格式。InnoDB有4种行格式：COMPACT、 REDUNDANT、DYNAMIC、COMPRESSED。

## COMPACT行格式

![[COMPACT行格式.png]]