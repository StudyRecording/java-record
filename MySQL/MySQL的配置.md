#MySQL #配置
## 类Unix操作系统中的配置文件

| 路径名              | 备注                                                         |
| ------------------- | ------------------------------------------------------------ |
| /etc/my.cnf         |                                                              |
| /etc/mysql/my.cnf   |                                                              |
| SYSCONFDIR/my.cnf   |                                                              |
| $MYSQL_HOME/my.cnf  | 特定于服务器的选项(仅限于服务器)                             |
| defaults-extra-file | 命令行指定的额外配置文件路径。<br />eg: `mysqld --defaults-extra-file=/home/admin/my_extra_file.txt` |
| ~/.my.cnf           | 特定于用户的选项                                             |
| ~/.mylogin.cnf      | 特定于用户的登录路径选项(仅限客户端)                         |

**注意事项：**

- SYSCONFDIR 表示在使用CMake构建MySQL时使用SYSCONFDIR选项指定的目录。
- MYSQL_HOME是用户可以自定义的环境变量。如果mysql_safe启动服务器程序，MYSQL_HOME默认为MySQL的安装目录。

## 配置文件内容

配置文件中的启动选项被划分为若干组，每个组有一个组名，用中括号[ ]括起来。如下示例：

```cnf
[server]
...
[mysqld]
....

...
```

### 程序的对应类别和能读取的组

| 程序名       | 类别       | 能读取的组                         |
| ------------ | ---------- | ---------------------------------- |
| mysqld       | 启动服务器 | [mysqld]、[server]                 |
| mysql_safe   | 启动服务器 | [mysqld]、[server]、[mysqld_safe]  |
| mysql.server | 启动服务器 | [mysqld]、[server]、[mysql.server] |
| mysql        | 启动客户端 | [mysql]、[client]                  |
| mysqladmin   | 启动客户端 | [mysqladmin]、[client]             |
| mysqldump    | 启动客户端 | [mysqldump]、[client]              |
