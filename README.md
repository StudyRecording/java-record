# JavaRecord
这是Java学习的相关知识点。因使用Obsidian编写，有些功能并不能在Gitee上正常显示，如若正常显示相关部分，建议拉取该仓库，并使用Obsidian软件正常导入该笔记库。

**注意：**

1. 对于tag中带有MindMap的都是是为导图，需要通过“命令面板”打开Mind Map进行浏览。 
    ![](./resources/MindMapUse.png.png)