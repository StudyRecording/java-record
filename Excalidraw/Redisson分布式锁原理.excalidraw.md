---

excalidraw-plugin: parsed

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
获取锁 ^VtZ1Rn0b

线程一 ^TdW9J7Sv

是否成功 ^2sVSOjzg

是 ^KbEwfp3i

执行业务 ^VW1gtykk

释放锁 ^exs7zclP

订阅redis消息 ^ygUvdJwp

不断自旋尝试获取锁(通过信号量阻塞释放cpu资源) ^ZNGu4AzZ

否 ^dDXtgGzh

检测持有锁线程的任务是否执行完 ^rNcquBfG

开启任务 ^McycymeV

锁续命 ^4d2OZ1MF

否 ^O7e9CbMF

释放锁时唤醒redis队列中的等待线程 ^ZkLBrW2q

关闭锁续命 ^DsIeSedM

是 ^sCqqTHfn

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "rectangle",
			"version": 111,
			"versionNonce": 489357745,
			"isDeleted": false,
			"id": "bhO5soyFhYxbwIQRgvG28",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -386.28125,
			"y": -111.98828125,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 112,
			"height": 66.7734375,
			"seed": 499758943,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "VtZ1Rn0b"
				},
				{
					"id": "429v2QZ5Q8EpxbHTBMTGI",
					"type": "arrow"
				},
				{
					"id": "4HU1zLcQCjUMEDzzEikjU",
					"type": "arrow"
				},
				{
					"id": "erhLYk41fvRPfgrRzd9hE",
					"type": "arrow"
				}
			],
			"updated": 1646296383396,
			"link": null
		},
		{
			"type": "text",
			"version": 85,
			"versionNonce": 1365264345,
			"isDeleted": false,
			"id": "VtZ1Rn0b",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -381.28125,
			"y": -91.1015625,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 102,
			"height": 25,
			"seed": 385040031,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943222,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "获取锁",
			"rawText": "获取锁",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "bhO5soyFhYxbwIQRgvG28",
			"originalText": "获取锁"
		},
		{
			"type": "arrow",
			"version": 116,
			"versionNonce": 1491354327,
			"isDeleted": false,
			"id": "429v2QZ5Q8EpxbHTBMTGI",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -500.30078125,
			"y": -74.51953125,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 113.01953125,
			"height": 1.7227412974642675,
			"seed": 1709404063,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943221,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "bhO5soyFhYxbwIQRgvG28",
				"gap": 1,
				"focus": -0.043528996423043795
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					113.01953125,
					-1.7227412974642675
				]
			]
		},
		{
			"type": "text",
			"version": 122,
			"versionNonce": 1268204849,
			"isDeleted": false,
			"id": "TdW9J7Sv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -478.31640625,
			"y": -117.08984375,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 60,
			"height": 28,
			"seed": 1107819391,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646294753961,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "线程一",
			"rawText": "线程一",
			"baseline": 21,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "线程一"
		},
		{
			"type": "diamond",
			"version": 174,
			"versionNonce": 819971761,
			"isDeleted": false,
			"id": "0hQ-deJjqs5TP0fGkrsua",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -160.09331597222229,
			"y": -130.01822916666669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 93.0234375,
			"height": 92.43359375,
			"seed": 1471772433,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "2sVSOjzg"
				},
				{
					"id": "4HU1zLcQCjUMEDzzEikjU",
					"type": "arrow"
				},
				{
					"id": "eDvvs9sOQiI6k9vj535u1",
					"type": "arrow"
				},
				{
					"id": "nDF6lG7Te6jOesa63sZ6S",
					"type": "arrow"
				},
				{
					"id": "hakWYZbOJbXVIXkhBhM8p",
					"type": "arrow"
				},
				{
					"id": "Dqy_Gwkf58dOUVkfUWZ6m",
					"type": "arrow"
				},
				{
					"id": "WajqFeJH_Vs0SDT0HSBCK",
					"type": "arrow"
				},
				{
					"id": "iOHaOTAIo8w9ujYN8nuEA",
					"type": "arrow"
				},
				{
					"id": "GLWFqYKGR4AoB134vqXaL",
					"type": "arrow"
				},
				{
					"id": "OAo9jLJaxUf9aht0JY_pM",
					"type": "arrow"
				}
			],
			"updated": 1646297705761,
			"link": null
		},
		{
			"type": "text",
			"version": 95,
			"versionNonce": 1062204823,
			"isDeleted": false,
			"id": "2sVSOjzg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -155.09331597222229,
			"y": -96.30143229166669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 83.0234375,
			"height": 25,
			"seed": 1750677823,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943224,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "是否成功",
			"rawText": "是否成功",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "0hQ-deJjqs5TP0fGkrsua",
			"originalText": "是否成功"
		},
		{
			"type": "arrow",
			"version": 233,
			"versionNonce": 630973625,
			"isDeleted": false,
			"id": "4HU1zLcQCjUMEDzzEikjU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -264.59765625,
			"y": -79.03401395128998,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 99.91324405603467,
			"height": 2.243174673554904,
			"seed": 772398737,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943223,
			"link": null,
			"startBinding": {
				"elementId": "bhO5soyFhYxbwIQRgvG28",
				"gap": 9.68359375,
				"focus": 0.03008388291851061
			},
			"endBinding": {
				"elementId": "0hQ-deJjqs5TP0fGkrsua",
				"gap": 5.2392719538876165,
				"focus": -0.0297926941726437
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					99.91324405603467,
					-2.243174673554904
				]
			]
		},
		{
			"type": "arrow",
			"version": 299,
			"versionNonce": 118172343,
			"isDeleted": false,
			"id": "eDvvs9sOQiI6k9vj535u1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -58.93348747270803,
			"y": -78.33514040328774,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 94.14095275048572,
			"height": 0.6141544653667381,
			"seed": 1143008017,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943224,
			"link": null,
			"startBinding": {
				"elementId": "0hQ-deJjqs5TP0fGkrsua",
				"gap": 9.8021020962819,
				"focus": 0.11056109943333792
			},
			"endBinding": {
				"elementId": "r3mDncmyk2P3CNVQN2mYZ",
				"gap": 5.148437500000028,
				"focus": 0.15208038650855968
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					94.14095275048572,
					0.6141544653667381
				]
			]
		},
		{
			"type": "text",
			"version": 27,
			"versionNonce": 5740625,
			"isDeleted": false,
			"id": "KbEwfp3i",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -25.9505208333334,
			"y": -124.47135416666669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20,
			"height": 28,
			"seed": 1197476095,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646297705762,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "是",
			"rawText": "是",
			"baseline": 21,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "是"
		},
		{
			"type": "rectangle",
			"version": 139,
			"versionNonce": 80318783,
			"isDeleted": false,
			"id": "r3mDncmyk2P3CNVQN2mYZ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 40.355902777777715,
			"y": -99.98307291666669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 127,
			"height": 53.71484375,
			"seed": 53823729,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "eDvvs9sOQiI6k9vj535u1",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "VW1gtykk"
				},
				{
					"id": "aTv26Q4eWKo_J2edab3gr",
					"type": "arrow"
				}
			],
			"updated": 1646297705762,
			"link": null
		},
		{
			"type": "text",
			"version": 61,
			"versionNonce": 429445401,
			"isDeleted": false,
			"id": "VW1gtykk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 45.355902777777715,
			"y": -85.62565104166669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 117,
			"height": 25,
			"seed": 1037346751,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943225,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "执行业务",
			"rawText": "执行业务",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "r3mDncmyk2P3CNVQN2mYZ",
			"originalText": "执行业务"
		},
		{
			"type": "arrow",
			"version": 158,
			"versionNonce": 7423481,
			"isDeleted": false,
			"id": "aTv26Q4eWKo_J2edab3gr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 171.35980902777771,
			"y": -72.01432291666669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 86.67097014002502,
			"height": 3.143339602276683,
			"seed": 861146559,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943226,
			"link": null,
			"startBinding": {
				"elementId": "r3mDncmyk2P3CNVQN2mYZ",
				"gap": 4.00390625,
				"focus": 0.1220670905060959
			},
			"endBinding": {
				"elementId": "v1yJHVLELmLWauVsSYNuk",
				"gap": 4.49699860997498,
				"focus": 0.2427227012734218
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					86.67097014002502,
					-3.143339602276683
				]
			]
		},
		{
			"type": "rectangle",
			"version": 81,
			"versionNonce": 407727647,
			"isDeleted": false,
			"id": "v1yJHVLELmLWauVsSYNuk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 262.5277777777777,
			"y": -95.87369791666669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 137,
			"height": 49.3125,
			"seed": 2071637343,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "exs7zclP"
				},
				{
					"id": "Lm3PeaF8VZ3xdBPmmEcCQ",
					"type": "arrow"
				},
				{
					"id": "aTv26Q4eWKo_J2edab3gr",
					"type": "arrow"
				},
				{
					"id": "pDMDQT89UJaLscK6h4Xiv",
					"type": "arrow"
				},
				{
					"id": "Y7nB8G9TvRTXt2b0djLmi",
					"type": "arrow"
				},
				{
					"id": "JMQBjapENMEWs_WDNEv12",
					"type": "arrow"
				}
			],
			"updated": 1646297823449,
			"link": null
		},
		{
			"type": "text",
			"version": 34,
			"versionNonce": 1646796505,
			"isDeleted": false,
			"id": "exs7zclP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 267.5277777777777,
			"y": -83.71744791666669,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 127,
			"height": 25,
			"seed": 1510164959,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943226,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "释放锁",
			"rawText": "释放锁",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "v1yJHVLELmLWauVsSYNuk",
			"originalText": "释放锁"
		},
		{
			"type": "arrow",
			"version": 1183,
			"versionNonce": 491838393,
			"isDeleted": false,
			"id": "OAo9jLJaxUf9aht0JY_pM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -110.27440118271383,
			"y": -31.777079856657735,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 8.835411303575015,
			"height": 176.35672395387996,
			"seed": 826999633,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943227,
			"link": null,
			"startBinding": {
				"elementId": "0hQ-deJjqs5TP0fGkrsua",
				"gap": 6.683206358203471,
				"focus": -0.015067038332864083
			},
			"endBinding": {
				"elementId": "xBJSSanUcnqBLY1WMogZi",
				"gap": 12.096354166666686,
				"focus": 0.1723748461797572
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					8.835411303575015,
					176.35672395387996
				]
			]
		},
		{
			"type": "rectangle",
			"version": 364,
			"versionNonce": 1358369791,
			"isDeleted": false,
			"id": "xBJSSanUcnqBLY1WMogZi",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -167.05403680695815,
			"y": 156.6759982638889,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 115.09114583333331,
			"height": 60,
			"seed": 1738681119,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "OAo9jLJaxUf9aht0JY_pM",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "ygUvdJwp"
				},
				{
					"id": "u1bISnvSKkSNKyP538hoM",
					"type": "arrow"
				},
				{
					"id": "EGKPVE2G1nAg7N7tUXPWh",
					"type": "arrow"
				},
				{
					"id": "Lm3PeaF8VZ3xdBPmmEcCQ",
					"type": "arrow"
				},
				{
					"id": "Y7nB8G9TvRTXt2b0djLmi",
					"type": "arrow"
				},
				{
					"id": "JMQBjapENMEWs_WDNEv12",
					"type": "arrow"
				}
			],
			"updated": 1646364713968,
			"link": null
		},
		{
			"type": "text",
			"version": 308,
			"versionNonce": 1913584793,
			"isDeleted": false,
			"id": "ygUvdJwp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -162.05403680695815,
			"y": 161.6759982638889,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 105.09114583333331,
			"height": 50,
			"seed": 752744255,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943227,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "订阅redis\n消息",
			"rawText": "订阅redis消息",
			"baseline": 43,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "xBJSSanUcnqBLY1WMogZi",
			"originalText": "订阅redis消息"
		},
		{
			"type": "rectangle",
			"version": 215,
			"versionNonce": 1302698385,
			"isDeleted": false,
			"id": "nYzl0oZhxBvO9aDRK-UgB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -421.8847659736245,
			"y": 36.284939236110745,
			"strokeColor": "#000000",
			"backgroundColor": "#40c057",
			"width": 163,
			"height": 135,
			"seed": 1553202687,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "ZNGu4AzZ"
				},
				{
					"id": "EGKPVE2G1nAg7N7tUXPWh",
					"type": "arrow"
				},
				{
					"id": "erhLYk41fvRPfgrRzd9hE",
					"type": "arrow"
				},
				{
					"id": "DOXqet0APKh929w7uFZmw",
					"type": "arrow"
				}
			],
			"updated": 1646374916323,
			"link": null
		},
		{
			"type": "text",
			"version": 324,
			"versionNonce": 1688824409,
			"isDeleted": false,
			"id": "ZNGu4AzZ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -416.8847659736245,
			"y": 53.784939236110745,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 153,
			"height": 100,
			"seed": 851530655,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943229,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "不断自旋尝试获\n取锁(通过信号量\n阻塞释放cpu资源\n)",
			"rawText": "不断自旋尝试获取锁(通过信号量阻塞释放cpu资源)",
			"baseline": 93,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "nYzl0oZhxBvO9aDRK-UgB",
			"originalText": "不断自旋尝试获取锁(通过信号量阻塞释放cpu资源)"
		},
		{
			"type": "arrow",
			"version": 510,
			"versionNonce": 1343619449,
			"isDeleted": false,
			"id": "erhLYk41fvRPfgrRzd9hE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -332.10624617063945,
			"y": 31.367404513888516,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.6681741151974165,
			"height": 64.8519209831332,
			"seed": 1606884447,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943228,
			"link": null,
			"startBinding": {
				"elementId": "nYzl0oZhxBvO9aDRK-UgB",
				"gap": 4.9175347222222285,
				"focus": 0.09164005963979918
			},
			"endBinding": {
				"elementId": "bhO5soyFhYxbwIQRgvG28",
				"gap": 11.730327280755318,
				"focus": 0.012281307389778747
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0.6681741151974165,
					-64.8519209831332
				]
			]
		},
		{
			"type": "text",
			"version": 77,
			"versionNonce": 1270300223,
			"isDeleted": false,
			"id": "dDXtgGzh",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -80.56098125140232,
			"y": 3.982421874999716,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20,
			"height": 28,
			"seed": 224084511,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "OAo9jLJaxUf9aht0JY_pM",
					"type": "arrow"
				}
			],
			"updated": 1646364700941,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "否",
			"rawText": "否",
			"baseline": 21,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "否"
		},
		{
			"type": "arrow",
			"version": 329,
			"versionNonce": 1700769431,
			"isDeleted": false,
			"id": "DOXqet0APKh929w7uFZmw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -114.35760762128507,
			"y": 75.88412099931764,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 142.14434585233943,
			"height": 14.601799644551278,
			"seed": 1197181087,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943228,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "nYzl0oZhxBvO9aDRK-UgB",
				"gap": 2.3828125,
				"focus": -0.06171100925790252
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-142.14434585233943,
					14.601799644551278
				]
			]
		},
		{
			"type": "line",
			"version": 709,
			"versionNonce": 131657439,
			"isDeleted": false,
			"id": "FSWLK7Xk5UyKQhypioeag",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -10.747648405220843,
			"y": -239.18495507083026,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.9744099657982588,
			"height": 152.22513317585825,
			"seed": 2059125969,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646297408272,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0.9744099657982588,
					152.22513317585825
				]
			]
		},
		{
			"type": "diamond",
			"version": 659,
			"versionNonce": 315607455,
			"isDeleted": false,
			"id": "53hZgaHANr8TYyFyLqx4C",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 31.123046526375674,
			"y": -338.59743923611154,
			"strokeColor": "#000000",
			"backgroundColor": "#4c6ef5",
			"width": 173.43315972222223,
			"height": 145.39496527777777,
			"seed": 707421247,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "rNcquBfG"
				},
				{
					"id": "uuxEmJdwIR9aZGWHgDHeM",
					"type": "arrow"
				},
				{
					"id": "dANsAZcfi-a4zK-CT-SvQ",
					"type": "arrow"
				},
				{
					"id": "v_s6yqF3VS-QJpKxTjD2r",
					"type": "arrow"
				},
				{
					"id": "Jfc1csDaSyK0lu1G1MLly",
					"type": "arrow"
				},
				{
					"id": "4D9gC-pH6twB351-q_fg_",
					"type": "arrow"
				},
				{
					"id": "X9xkmrmHLJOwpd4E1SYb7",
					"type": "arrow"
				}
			],
			"updated": 1646374716398,
			"link": null
		},
		{
			"type": "text",
			"version": 542,
			"versionNonce": 647244567,
			"isDeleted": false,
			"id": "rNcquBfG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 36.123046526375674,
			"y": -290.8999565972226,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 163.43315972222223,
			"height": 50,
			"seed": 790392351,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943231,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "检测持有锁线程的\n任务是否执行完",
			"rawText": "检测持有锁线程的任务是否执行完",
			"baseline": 43,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "53hZgaHANr8TYyFyLqx4C",
			"originalText": "检测持有锁线程的任务是否执行完"
		},
		{
			"type": "arrow",
			"version": 921,
			"versionNonce": 1755100985,
			"isDeleted": false,
			"id": "uuxEmJdwIR9aZGWHgDHeM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -42.07114280991537,
			"y": -262.95016359039823,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 69.59586880697357,
			"height": 2.0471550085944727,
			"seed": 832123537,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943230,
			"link": null,
			"startBinding": {
				"elementId": "McycymeV",
				"gap": 15.073784722222172,
				"focus": 1.2880247990261775
			},
			"endBinding": {
				"elementId": "53hZgaHANr8TYyFyLqx4C",
				"gap": 3.7098067318333605,
				"focus": 0.024126902833008945
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					69.59586880697357,
					-2.0471550085944727
				]
			]
		},
		{
			"type": "text",
			"version": 218,
			"versionNonce": 1617388433,
			"isDeleted": false,
			"id": "McycymeV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -136.62000902917998,
			"y": -294.8934461805558,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 80,
			"height": 28,
			"seed": 1785253887,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "uuxEmJdwIR9aZGWHgDHeM",
					"type": "arrow"
				}
			],
			"updated": 1646364763264,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "开启任务",
			"rawText": "开启任务",
			"baseline": 21,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "开启任务"
		},
		{
			"type": "rectangle",
			"version": 160,
			"versionNonce": 1237508927,
			"isDeleted": false,
			"id": "GVai6e3Iom_-MiKWnOFjs",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 305.84092847082013,
			"y": -286.6573350694447,
			"strokeColor": "#000000",
			"backgroundColor": "#7950f2",
			"width": 155,
			"height": 58.21614583333334,
			"seed": 1281723167,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "4d2OZ1MF"
				},
				{
					"id": "dANsAZcfi-a4zK-CT-SvQ",
					"type": "arrow"
				},
				{
					"id": "v_s6yqF3VS-QJpKxTjD2r",
					"type": "arrow"
				}
			],
			"updated": 1646297936205,
			"link": null
		},
		{
			"type": "text",
			"version": 88,
			"versionNonce": 1622643031,
			"isDeleted": false,
			"id": "4d2OZ1MF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 310.84092847082013,
			"y": -270.049262152778,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 145,
			"height": 25,
			"seed": 112699807,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943234,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "锁续命",
			"rawText": "锁续命",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "GVai6e3Iom_-MiKWnOFjs",
			"originalText": "锁续命"
		},
		{
			"type": "arrow",
			"version": 688,
			"versionNonce": 858566711,
			"isDeleted": false,
			"id": "dANsAZcfi-a4zK-CT-SvQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 209.09606623699085,
			"y": -262.1668582318813,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 95.499202511607,
			"height": 0.4032059439380191,
			"seed": 681233151,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943232,
			"link": null,
			"startBinding": {
				"elementId": "53hZgaHANr8TYyFyLqx4C",
				"gap": 5.87761449225151,
				"focus": 0.04605119205053896
			},
			"endBinding": {
				"elementId": "GVai6e3Iom_-MiKWnOFjs",
				"gap": 1.2456597222222854,
				"focus": 0.13187978820529486
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					95.499202511607,
					0.4032059439380191
				]
			]
		},
		{
			"type": "text",
			"version": 12,
			"versionNonce": 724424383,
			"isDeleted": false,
			"id": "O7e9CbMF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 248.65776874859785,
			"y": -284.2771267361113,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20,
			"height": 28,
			"seed": 1181401617,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646374485692,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "否",
			"rawText": "否",
			"baseline": 21,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "否"
		},
		{
			"type": "arrow",
			"version": 873,
			"versionNonce": 1734267577,
			"isDeleted": false,
			"id": "v_s6yqF3VS-QJpKxTjD2r",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 380.237629859709,
			"y": -292.58181423611114,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 271.56975014160605,
			"height": 127.63474858104935,
			"seed": 559087583,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943232,
			"link": null,
			"startBinding": {
				"elementId": "GVai6e3Iom_-MiKWnOFjs",
				"gap": 5.9244791666664725,
				"focus": -0.03238597264552393
			},
			"endBinding": {
				"elementId": "53hZgaHANr8TYyFyLqx4C",
				"gap": 2.4613887658683495,
				"focus": -0.01951539669135752
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-2.0958648717517665,
					-127.06617130135005
				],
				[
					-271.56975014160605,
					-127.63474858104935
				],
				[
					-267.559763192119,
					-44.900249096909135
				]
			]
		},
		{
			"type": "text",
			"version": 111,
			"versionNonce": 465855793,
			"isDeleted": false,
			"id": "ZkLBrW2q",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 69.45637985970973,
			"y": 200.44509548611103,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 326,
			"height": 28,
			"seed": 704107039,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646297705762,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "释放锁时唤醒redis队列中的等待线程",
			"rawText": "释放锁时唤醒redis队列中的等待线程",
			"baseline": 21,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "释放锁时唤醒redis队列中的等待线程"
		},
		{
			"type": "arrow",
			"version": 312,
			"versionNonce": 1099339863,
			"isDeleted": false,
			"id": "JMQBjapENMEWs_WDNEv12",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 340.51540763748847,
			"y": -36.27539062500176,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 390.9027777777778,
			"height": 216.94010416666674,
			"seed": 676049215,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943227,
			"link": null,
			"startBinding": {
				"elementId": "v1yJHVLELmLWauVsSYNuk",
				"gap": 10.285807291664923,
				"focus": -0.153574460499022
			},
			"endBinding": {
				"elementId": "xBJSSanUcnqBLY1WMogZi",
				"gap": 1.5755208333354744,
				"focus": -0.2597460493330218
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-7.1875,
					216.94010416666674
				],
				[
					-390.9027777777778,
					215.34288194444446
				]
			]
		},
		{
			"type": "rectangle",
			"version": 237,
			"versionNonce": 1518550463,
			"isDeleted": false,
			"id": "Auh-tGsIuS2LZYHVDv4lm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 314.28049027707635,
			"y": -176.48903227663578,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 158.28993055555566,
			"height": 57.3524305555556,
			"seed": 358071569,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "4D9gC-pH6twB351-q_fg_",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "DsIeSedM"
				},
				{
					"id": "X9xkmrmHLJOwpd4E1SYb7",
					"type": "arrow"
				}
			],
			"updated": 1646374716398,
			"link": null
		},
		{
			"type": "text",
			"version": 104,
			"versionNonce": 411651703,
			"isDeleted": false,
			"id": "DsIeSedM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 319.28049027707635,
			"y": -160.31281699885798,
			"strokeColor": "#000000",
			"backgroundColor": "#7950f2",
			"width": 148.28993055555566,
			"height": 25,
			"seed": 159175455,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943235,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "关闭锁续命",
			"rawText": "关闭锁续命",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "Auh-tGsIuS2LZYHVDv4lm",
			"originalText": "关闭锁续命"
		},
		{
			"type": "arrow",
			"version": 171,
			"versionNonce": 173160569,
			"isDeleted": false,
			"id": "X9xkmrmHLJOwpd4E1SYb7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 124.79567795930403,
			"y": -189.88797866791236,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 185.68201325216822,
			"height": 45.88034824778561,
			"seed": 1595319487,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "sCqqTHfn"
				}
			],
			"updated": 1646640943236,
			"link": null,
			"startBinding": {
				"elementId": "53hZgaHANr8TYyFyLqx4C",
				"gap": 5.408091404318618,
				"focus": 0.09378589923578504
			},
			"endBinding": {
				"elementId": "Auh-tGsIuS2LZYHVDv4lm",
				"gap": 1.878309374649291,
				"focus": 0.028035630753913625
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					7.995942734370574,
					45.88034824778561
				],
				[
					185.68201325216822,
					42.40776668450968
				]
			]
		},
		{
			"type": "text",
			"version": 17,
			"versionNonce": 627468183,
			"isDeleted": false,
			"id": "sCqqTHfn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 129.79567795930404,
			"y": -179.44780454401956,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 175.68201325216822,
			"height": 25,
			"seed": 440042015,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646640943236,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "是",
			"rawText": "是",
			"baseline": 18,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "X9xkmrmHLJOwpd4E1SYb7",
			"originalText": "是"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "sharp",
		"gridSize": null
	},
	"files": {}
}
```
%%